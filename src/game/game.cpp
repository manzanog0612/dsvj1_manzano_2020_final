#include "game.h"

#include <time.h>
#include <stdlib.h>

#include "raylib.h"

#include "grid.h"
#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"

using namespace game;
using namespace grid;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;

namespace game
{
	void initialization()
	{
		title.text = "Slippery Painter Boi";
		screenWidth = 1024.0f;
		screenHeight = 562.0f;

		InitWindow(static_cast<int>(screenWidth), static_cast<int>(screenHeight), title.text);
		InitAudioDevice();
		SetTargetFPS(fps);

		srand(static_cast<unsigned int>(time(NULL)));

		textInitialization();
		restartGame();
	}

	void input()
	{
		if (IsKeyPressed(KEY_R))
			restart = true;

		if ((IsKeyPressed(playerControls.right) || IsKeyPressed(playerControls.left) ||
			IsKeyPressed(playerControls.up) || IsKeyPressed(playerControls.down)) &&
			!player.moving)
				setPlayerInput();
	}

	void update()
	{
		if (restart)
		{
			restart = false;
			restartGame();
			remainingMovements = 10;
			win = false;
			lose = false;
		}

		if (!win && !lose)
		{
			if (player.moving)
			{
				movePlayer();
				paintTiles();
			}

			if (checkVictory() && !player.moving)
				win = true;

			if (!win && remainingMovements == 0 && !player.moving)
				lose = true;
		}
	}

	void draw()
	{
		BeginDrawing();

		ClearBackground(BLACK);

		drawGrid();
		drawRemainingMovements();
		drawTilesInformation();

		if (win || lose)
			drawResultScreen();

		EndDrawing();
	}

	void deinitialization()
	{
		
	}

	void run()
	{
		game::initialization();

		while (!WindowShouldClose() && playingGame)
		{
			game::input();
			game::update();
			game::draw();
		}

		deinitialization();

		CloseWindow();		
	}
}