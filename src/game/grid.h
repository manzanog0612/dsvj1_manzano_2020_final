#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "game_vars/global_vars.h"

using namespace global_vars;

namespace game
{
	namespace grid
	{
		enum class TILES { WALL, FLOOR, PAINTEDFLOOR };

		struct Tiles
		{
			TILES type;
			short row;
			short column;
			Rectangle dimentions;
			Color color;
		};

		struct Player
		{
			short row;
			short column;
			Rectangle dimentions;
			Color color;
			bool moving;
			bool movementInX;
			int lastMovement;
		};

		extern Tiles grid[amountRows][amountColumns];
		extern Player player;

		void restartGrid();

		void restartGame();

		void drawResultScreen();

		void textInitialization();

		void setPlayerInput();

		void movePlayer();

		void paintTiles();

		bool checkVictory();

		void drawRemainingMovements();

		void drawTilesInformation();

		void drawGrid();
	}
}

#endif // GAMEPLAY_H
