#include "grid.h"

#include <cmath>
#include <vector>
#include <iostream>

#include "raylib.h"

#include "game_vars/global_vars.h"
#include "game_vars/global_drawing_vars.h"
#include "configurations/configurations.h"

using namespace game;
using namespace global_vars;
using namespace global_drawing_vars;
using namespace configurations;

namespace game
{
	namespace grid
	{		
		Tiles grid[amountRows][amountColumns];
		Player player;

		//animation vars
		short auxIndex[2];
		float startPos;
		float endPos;
		float timeLaps;

		void restartGrid()
		{
			Rectangle gameSpace;
			TILES auxLevel[amountRows][amountColumns] = {   {TILES::WALL, TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL},
															{TILES::WALL, TILES::PAINTEDFLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::WALL, TILES::WALL},
															{TILES::WALL, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::WALL},
															{TILES::WALL, TILES::FLOOR, TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::FLOOR, TILES::FLOOR, TILES::WALL},
															{TILES::WALL, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::WALL},
															{TILES::WALL, TILES::FLOOR, TILES::FLOOR, TILES::FLOOR, TILES::WALL,  TILES::FLOOR, TILES::FLOOR, TILES::WALL},
															{TILES::WALL, TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::FLOOR, TILES::FLOOR, TILES::WALL},
															{TILES::WALL, TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL,  TILES::WALL}
														};

			gameSpace.width = screenWidth / 3.f;
			gameSpace.height = screenHeight / 1.5f;
			gameSpace.x = (screenWidth - gameSpace.width) / 2.0f;
			gameSpace.y = (screenHeight - gameSpace.height) / 2.0f;

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					grid[i][j].dimentions.width = gameSpace.width / amountColumns;
					grid[i][j].dimentions.height = gameSpace.height / amountRows;
					grid[i][j].dimentions.x = gameSpace.x + j * (grid[i][j].dimentions.width);
					grid[i][j].dimentions.y = gameSpace.y + i * (grid[i][j].dimentions.height);
					grid[i][j].column = j;
					grid[i][j].row = i;

					grid[i][j].type = auxLevel[i][j];

					switch (grid[i][j].type)
					{
					case game::grid::TILES::WALL:
						grid[i][j].color = DARKGRAY;
						break;
					case game::grid::TILES::FLOOR:
						grid[i][j].color = LIGHTGRAY;
						break;
					case game::grid::TILES::PAINTEDFLOOR:
						grid[i][j].color = RED;
						break;
					default:
						break;
					}
				}
			}
		}

		void restartGame()
		{
			restartGrid();
			player.color = DARKPURPLE;
			player.dimentions.width = grid[0][0].dimentions.width;
			player.dimentions.height = grid[0][0].dimentions.height;
			player.dimentions.x = grid[1][1].dimentions.x;
			player.dimentions.y = grid[1][1].dimentions.y;
			player.row = 1;
			player.column = 1;
			player.moving = false;
			player.movementInX = false;
		}

		void drawResultScreen()
		{
			if (win)
			{
				resultText[0].text = "You win";
				resultText[1].text = "Congratulations!";
			}
			else
			{
				resultText[0].text = "You lose";
				resultText[1].text = "Try again";
			}

			pressR.text = "Press R to restart the game";

			resultText[0].fontSize = static_cast<int>(30.0f);
			resultText[1].fontSize = static_cast<int>(30.0f);
			pressR.fontSize = pressEnter.fontSize;

			resultText[0].posY = static_cast<int>(grid[0][0].dimentions.y - resultText[0].fontSize * 3 );
			resultText[1].posY = static_cast<int>(grid[0][0].dimentions.y - resultText[0].fontSize * 2);
			pressR.posY = static_cast<int>(grid[7][0].dimentions.y + resultText[0].fontSize * 2);

			DrawText(resultText[0].text, (static_cast<int>(screenWidth) - MeasureText(resultText[0].text, resultText[0].fontSize)) / 2,
				resultText[0].posY, resultText[0].fontSize, YELLOW);

			DrawText(resultText[1].text, (static_cast<int>(screenWidth) - MeasureText(resultText[1].text, resultText[1].fontSize)) / 2,
				resultText[1].posY, resultText[1].fontSize, YELLOW);

			DrawText(pressR.text, (screenLimit.right - MeasureText(pressR.text, pressR.fontSize)) / 2, pressR.posY, pressR.fontSize, MAGENTA);
		}

		void textInitialization()
		{
			pause.text = "Pause";
			pressEnter.text = "Press enter to go back to menu";
			pressP.text = "Press P to coninue";
			pressR.text = "Press R to restart the game";

			pause.fontSize = static_cast<int>(80.0f);
			pressEnter.fontSize = static_cast<int>(25.0f);
			pressP.fontSize = pressEnter.fontSize;
			pressR.fontSize = pressEnter.fontSize;

			pause.posY = static_cast<int>(screenHeight) / 2 - pause.fontSize / 2;
			pressEnter.posY = screenLimit.down - pressEnter.fontSize * 2;
			pressP.posY = pause.posY - pressP.fontSize * 3;
			pressR.posY = pause.posY + pause.fontSize + pause.fontSize / 2;

			restartGrid();
		}

		void setPlayerInput()
		{
			auxIndex[0] = player.row;
			auxIndex[1] = player.column;

			if (player.moving) return;

			if (IsKeyPressed(playerControls.up))
			{
				while (grid[player.row - 1][player.column].type != TILES::WALL)
				{
					player.row--;
				}

				if (player.movementInX)
					player.movementInX = false;

				player.lastMovement = playerControls.up;
			}
			else if (IsKeyPressed(playerControls.down))
			{
				while (grid[player.row + 1][player.column].type != TILES::WALL)
				{
					player.row++;
				}

				if (player.movementInX)
					player.movementInX = false;

				player.lastMovement = playerControls.down;
			}
			else if (IsKeyPressed(playerControls.left))
			{
				while (grid[player.row][player.column - 1].type != TILES::WALL)
				{
					player.column--;
				}

				if (!player.movementInX)
					player.movementInX = true;

				player.lastMovement = playerControls.left;
			}
			else if (IsKeyPressed(playerControls.right))
			{
				while (grid[player.row][player.column + 1].type != TILES::WALL)
				{
					player.column++;
				}

				if (!player.movementInX)
					player.movementInX = true;

				player.lastMovement = playerControls.right;
			}

			if (auxIndex[0] != player.row || auxIndex[1] != player.column)
			{
				player.moving = true;
				remainingMovements--;
			}
		}

		static float lerp(float a, float b, float f)
		{
			return (a * (1.0f - f)) + (b * f);
		}

		void movePlayer()
		{
			if (player.movementInX)
			{
				if (startPos == 0.0f)
					startPos = grid[auxIndex[0]][auxIndex[1]].dimentions.x;

				if (endPos == 0.0f)
					endPos = grid[player.row][player.column].dimentions.x;
			}
			else
			{
				if (startPos == 0.0f)
					startPos = grid[auxIndex[0]][auxIndex[1]].dimentions.y;

				if (endPos == 0.0f)
					endPos = grid[player.row][player.column].dimentions.y;
			}

			if (timeLaps < 1.0f)
			{
				timeLaps += 0.05f;
			}

			if (player.movementInX)
				player.dimentions.x = lerp(startPos, endPos, timeLaps);
			else
				player.dimentions.y = lerp(startPos, endPos, timeLaps);

			if (timeLaps > 1.0f)
			{
				player.moving = false;
				timeLaps = 0.0f;
				startPos = 0.0f;
				endPos = 0.0f;
				player.dimentions.x = grid[player.row][player.column].dimentions.x;
				player.dimentions.y = grid[player.row][player.column].dimentions.y;
			}
		}

		void paintTiles()
		{
			if (player.movementInX)
			{
				for (short i = 0; i < amountRows; i++)
				{
					for (short j = 0; j < amountColumns; j++)
					{
						if (player.lastMovement == playerControls.right)
						{
							if (player.dimentions.x + player.dimentions.width >= grid[i][j].dimentions.x && player.dimentions.x <= grid[i][j].dimentions.x &&
								i == player.row && grid[i][j].type == TILES::FLOOR)
							{
								grid[i][j].type = TILES::PAINTEDFLOOR;
								grid[i][j].color = RED;
							}
						}
						else
						{
							if (player.dimentions.x <= grid[i][j].dimentions.x + grid[i][j].dimentions.width && 
								player.dimentions.x + player.dimentions.width >= grid[i][j].dimentions.x + grid[i][j].dimentions.width &&
								i == player.row && grid[i][j].type == TILES::FLOOR)
							{
								grid[i][j].type = TILES::PAINTEDFLOOR;
								grid[i][j].color = RED;
							}
						}
					}
				}
			}
			else
			{
				for (short i = 0; i < amountRows; i++)
				{
					for (short j = 0; j < amountColumns; j++)
					{
						if (player.lastMovement == playerControls.down)
						{
							if (player.dimentions.y + player.dimentions.height >= grid[i][j].dimentions.y && player.dimentions.y <= grid[i][j].dimentions.y &&
								j == player.column && grid[i][j].type == TILES::FLOOR)
							{
								grid[i][j].type = TILES::PAINTEDFLOOR;
								grid[i][j].color = RED;
							}
						}
						else
						{
							if (player.dimentions.y <= grid[i][j].dimentions.y + grid[i][j].dimentions.height &&
								player.dimentions.y + player.dimentions.height >= grid[i][j].dimentions.y + grid[i][j].dimentions.height &&
								j == player.column && grid[i][j].type == TILES::FLOOR)
							{
								grid[i][j].type = TILES::PAINTEDFLOOR;
								grid[i][j].color = RED;
							}
						}
					}
				}
			}
		}

		bool checkVictory()
		{
			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (grid[i][j].type == TILES::FLOOR)
						return false;
				}
			}

			return true;
		}

		void drawRemainingMovements()
		{
			movementsLeft.text = "Remaining Movements: ";
			movementsLeft.fontSize = 20;
			movementsLeft.posY = static_cast<int>(grid[0][0].dimentions.y);

			DrawText(movementsLeft.text, grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10, movementsLeft.posY, movementsLeft.fontSize, YELLOW);
			DrawText(FormatText("%i", remainingMovements), (grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10 + MeasureText(movementsLeft.text, movementsLeft.fontSize)), movementsLeft.posY, movementsLeft.fontSize, YELLOW);
		}

		void drawTilesInformation()
		{
			short painted = 0;
			short unpainted = 0;

			paintedTiles.text = "Painted tiles: ";
			floorTiles.text = "Remaining tiles: ";
			paintedTiles.posY = movementsLeft.posY + movementsLeft.fontSize + 10;
			floorTiles.posY = movementsLeft.posY + movementsLeft.fontSize * 2 + 20;

			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					if (grid[i][j].type == TILES::FLOOR)
					{
						unpainted++;
					}
					else if (grid[i][j].type == TILES::PAINTEDFLOOR)
					{
						painted++;
					}
				}
			}

			DrawText(paintedTiles.text, grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10, paintedTiles.posY, movementsLeft.fontSize, YELLOW);
			DrawText(FormatText("%i", painted), (grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10 + MeasureText(paintedTiles.text, movementsLeft.fontSize)), paintedTiles.posY, movementsLeft.fontSize, YELLOW);
			DrawText(floorTiles.text, grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10, floorTiles.posY, movementsLeft.fontSize, YELLOW);
			DrawText(FormatText("%i", unpainted), (grid[0][7].dimentions.x + grid[0][7].dimentions.width + 10 + MeasureText(floorTiles.text, movementsLeft.fontSize)), floorTiles.posY, movementsLeft.fontSize, YELLOW);
		}

		void drawGrid()
		{
			for (short i = 0; i < amountRows; i++)
			{
				for (short j = 0; j < amountColumns; j++)
				{
					DrawRectangleRec(grid[i][j].dimentions, grid[i][j].color);
					DrawRectangleLinesEx({ grid[i][j].dimentions.x, grid[i][j].dimentions.y, grid[i][j].dimentions.width + 2, grid[i][j].dimentions.height + 2 }, 1, BLACK);
					DrawRectangleRec(player.dimentions, player.color);
					DrawRectangleLinesEx({ player.dimentions.x, player.dimentions.y, player.dimentions.width + 2, player.dimentions.height + 2 }, 1, MAGENTA);
				}
			}
		}
	}
}
