#ifndef GAME_H
#define GAME_H

namespace game
{
	void initialization();

	void input();

	void update();

	void draw();

	void deinitialization();

	void run();
}

#endif //GAME_H
