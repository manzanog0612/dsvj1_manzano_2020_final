#ifndef GLOBAL_VARS_H
#define GLOBAL_VARS_H

#include "raylib.h"

#include "game/game.h"
#include "configurations/configurations.h"

using namespace game;
using namespace configurations;

namespace game
{
	namespace global_vars
	{
		const short amountColumns = 8;
		const short amountRows = 8;

		extern short fps;

		extern Mouse mouse;

		extern bool pauseGame;

		extern bool restart;

		extern bool win;
		extern bool lose;

		extern bool playingGame;

		extern short remainingMovements;
	}
}
#endif //GLOBAL_VARS_H
