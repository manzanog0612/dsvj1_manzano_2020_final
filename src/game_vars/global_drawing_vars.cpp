#include "game_vars/global_drawing_vars.h"

#include "raylib.h"

#include "configurations/configurations.h"

using namespace game;
using namespace configurations;

namespace game
{
	namespace global_drawing_vars
	{
		Words title;

		Words score;

		Words pause;
		Words pressP;
		Words pressR;
		Words pressEnter;

		Words resultText[2];
		Words movementsLeft;
		Words paintedTiles;
		Words floorTiles;

		Words youLost;
	}
}