#include "game_vars/global_vars.h"

#include "game_vars/global_drawing_vars.h"

using namespace game;
using namespace global_drawing_vars;

namespace game
{
	namespace global_vars
	{
		short fps = 60;

		Mouse mouse;

		bool pauseGame = false;

		bool restart = false;

		bool win = false;
		bool lose = false;

		bool playingGame = true;

		short remainingMovements = 10;
	}
}
