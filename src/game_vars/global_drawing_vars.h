#ifndef GLOBAL_DRAWING_VARS_H
#define GLOBAL_DRAWING_VARS_H

#include "raylib.h"

namespace game
{
	namespace global_drawing_vars
	{
		struct Words
		{
			const char* text;
			int posY;
			int  fontSize;
		};

		extern Words title;

		extern Words score;

		extern Words pause;
		extern Words pressP;
		extern Words pressR;
		extern Words pressEnter;

		extern Words resultText[2];
		extern Words movementsLeft;
		extern Words paintedTiles;
		extern Words floorTiles;

		extern Words youLost;
	}
}

#endif //GLOBAL_DRAWING_VARS_H
