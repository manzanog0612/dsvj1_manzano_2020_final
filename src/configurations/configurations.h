#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

#include "raylib.h"

namespace configurations
{
	struct Mouse
	{
		int posX;
		int posY;
	};

	extern float screenWidth;
	extern float screenHeight;

	struct ScreenLimit
	{
		int right = static_cast<int>(screenWidth);
		int left = 0;
		int up = 0;
		int down = static_cast<int>(screenHeight);
	};

	struct Controls
	{
		int up = KEY_W;
		int down = KEY_S;
		int left = KEY_A;
		int right = KEY_D;
	};

	extern ScreenLimit screenLimit;

	extern Controls playerControls;
}

#endif //CONFIGURATIONS_H
